package com.example.pial.audiomvpmodel.Audio;

import android.media.MediaPlayer;
import android.media.MediaRecorder;

/**
 * Created by pial on 7/24/17.
 */

public interface AudioInterface {
    interface view
    {
        void recordingStartException(Exception e);
        void recordingStartMessage();
        void recordingCompleteMessage();
        void playLastRecordAudioException(Exception e);
        void playLastRecordAudioMessage();
        void stopPlayingRecordingMessage();
        void buttonStartEnable();
        void buttonStartDisable();
        void buttonStopEnable();
        void buttonStopDisable();
        void buttonPlayLastRecordAudioEnable();
        void buttonPlayLastRecordAudioDisable();
        void buttonStopPlayingRecordingEnable();
        void buttonStopPlayingRecordingDisable();


    }
    interface presenter
    {
        void onButtonStart(String audioSavePathInDevice);
        void onButtonStop();
        void onButtonPlayLastRecordAudio();
        void onButtonStopPlayingRecording();
        void setView(AudioInterface.view view);
    }
    interface model
    {
        void setAudioPath(String path);
        String getAudioRepository();
        void setAudioReacording(MediaRecorder mediaRecorder);
        MediaRecorder getAudioRecording();
        void setMediaplayer(MediaPlayer mediaPlayer);
        MediaPlayer getAudioMediaPlayer();

    }
}
