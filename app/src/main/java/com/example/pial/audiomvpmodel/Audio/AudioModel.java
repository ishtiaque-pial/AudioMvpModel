package com.example.pial.audiomvpmodel.Audio;

import android.media.MediaPlayer;
import android.media.MediaRecorder;
import dagger.Module;

/**
 * Created by pial on 7/25/17.
 */

public class AudioModel implements AudioInterface.model {

    private AudioRepository audioRepository;

    public AudioModel(AudioRepository audioRepository) {
        this.audioRepository =audioRepository;
    }

    @Override
    public void setAudioPath(String path) {
        audioRepository.setPath(path);
    }

    public String getAudioRepository() {


        return audioRepository.getPath();
    }

    @Override
    public void setAudioReacording(MediaRecorder mediaRecorder) {
        audioRepository.setMediaRecorder(mediaRecorder);

    }

    @Override
    public MediaRecorder getAudioRecording() {
        return audioRepository.getMediaRecorder();
    }

    @Override
    public void setMediaplayer(MediaPlayer mediaPlayer) {
        audioRepository.setMediaPlayer(mediaPlayer);

    }

    @Override
    public MediaPlayer getAudioMediaPlayer() {
        return audioRepository.getMediaPlayer();
    }
}
