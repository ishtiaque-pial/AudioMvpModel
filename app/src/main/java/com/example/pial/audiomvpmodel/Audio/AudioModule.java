package com.example.pial.audiomvpmodel.Audio;

import android.media.MediaPlayer;
import android.media.MediaRecorder;

import dagger.Module;
import dagger.Provides;

/**
 * Created by pial on 7/27/17.
 */
@Module
public class AudioModule {

    private AudioInterface.model model;
    private AudioRepository audioRepository;
    private MediaRecorder mediaRecorder;
    private MediaPlayer mediaPlayer;

    public AudioModule(AudioModel Model,AudioRepository audioRepository,MediaRecorder mediaRecorder,MediaPlayer mediaPlayer) {

        this.model=Model;
        this.audioRepository=audioRepository;
        this.mediaRecorder=mediaRecorder;
        this.mediaPlayer=mediaPlayer;
    }

    @Provides
    public AudioInterface.presenter provideAudioPresenter()
    {
        return new AudioPresenter(model,mediaRecorder,mediaPlayer);
    }

    @Provides
    public AudioRepository provideAudioRepository()
    {
        return audioRepository;
    }

}
