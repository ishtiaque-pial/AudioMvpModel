package com.example.pial.audiomvpmodel.Audio;

import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.util.Log;

/**
 * Created by pial on 7/25/17.
 */

public class AudioPresenter implements AudioInterface.presenter {
    private AudioInterface.model model;
    private AudioInterface.view view;
    private MediaRecorder mediaRecorder;
    private MediaPlayer mediaPlayer;

    public AudioPresenter(AudioInterface.model model,MediaRecorder mediaRecorder,MediaPlayer mediaPlayer) {
        this.model = model;
        this.mediaRecorder=mediaRecorder;
        this.mediaPlayer=mediaPlayer;
    }

    @Override
    public void onButtonStart(String audioSavePathInDevice) {
        try{
            model.setAudioPath(audioSavePathInDevice);
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
            mediaRecorder.setOutputFile(model.getAudioRepository());
            mediaRecorder.prepare();
            mediaRecorder.start();
            view.recordingStartMessage();
            model.setAudioReacording(mediaRecorder);
            view.buttonStartDisable();
            view.buttonStopEnable();
        } catch (Exception e) {
            view.recordingStartException(e);


        }

    }

    @Override
    public void onButtonStop() {
        model.getAudioRecording().stop();
        view.recordingCompleteMessage();
        view.buttonStopDisable();
        view.buttonPlayLastRecordAudioEnable();
        view.buttonStartEnable();
        view.buttonStopPlayingRecordingDisable();

    }

    @Override
    public void onButtonPlayLastRecordAudio() {
            try {
                mediaPlayer.setDataSource(model.getAudioRepository());
                mediaPlayer.prepare();
                mediaPlayer.start();
                model.setMediaplayer(mediaPlayer);
                view.playLastRecordAudioMessage();
                view.buttonStopDisable();
                view.buttonStartDisable();
                view.buttonStopPlayingRecordingEnable();

            } catch (Exception e) {
                view.playLastRecordAudioException(e);
            }

    }

    @Override
    public void onButtonStopPlayingRecording() {
        if (model.getAudioMediaPlayer()!=null)
        {
            model.getAudioMediaPlayer().stop();
            model.getAudioMediaPlayer().release();
            view.stopPlayingRecordingMessage();
            view.buttonStopDisable();
            view.buttonStartEnable();
            view.buttonStopPlayingRecordingDisable();
            view.buttonPlayLastRecordAudioEnable();
        }

    }

    @Override
    public void setView(AudioInterface.view view) {
        this.view=view;

    }


}
