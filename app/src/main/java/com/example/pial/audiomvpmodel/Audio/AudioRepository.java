package com.example.pial.audiomvpmodel.Audio;

import android.media.MediaPlayer;
import android.media.MediaRecorder;

/**
 * Created by pial on 7/24/17.
 */

public class AudioRepository {
    private String path;
    private MediaRecorder mediaRecorder;
    private MediaPlayer mediaPlayer;

    public MediaRecorder getMediaRecorder() {
        return mediaRecorder;
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    public void setMediaRecorder(MediaRecorder mediaRecorder) {
        this.mediaRecorder = mediaRecorder;
    }

    public void setMediaPlayer(MediaPlayer mediaPlayer) {
        this.mediaPlayer = mediaPlayer;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
