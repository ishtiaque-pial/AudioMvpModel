package com.example.pial.audiomvpmodel.Audio;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.pial.audiomvpmodel.R;
import com.example.pial.audiomvpmodel.Root.App;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements AudioInterface.view {

    @Inject
    AudioInterface.presenter presenter;
    @BindView(R.id.button)
    Button buttonStart;
    @BindView(R.id.button2)
    Button buttonStop;
    @BindView(R.id.button3)
    Button buttonPlayLastRecordAudio;
    @BindView(R.id.button4)
    Button buttonStopPlayingRecording;
    private String AudioSavePathInDevice = null;
    private String dir;
    private File newdir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((App) getApplication()).getComponent().inject(this);
        ButterKnife.bind(this);
        buttonStop.setEnabled(false);
        buttonPlayLastRecordAudio.setEnabled(false);
        buttonStopPlayingRecording.setEnabled(false);
        dir = Environment.getExternalStorageDirectory() + "/Grassroot/";
        newdir = new File(dir);
        newdir.mkdirs();

        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                AudioSavePathInDevice =dir+timeStamp+".3gp";
                presenter.onButtonStart(AudioSavePathInDevice);

            }
        });
        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onButtonStop();

            }
        });
        buttonPlayLastRecordAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                presenter.onButtonPlayLastRecordAudio();

            }
        });
        buttonStopPlayingRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onButtonStopPlayingRecording();

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.setView(this);
    }


    @Override
    public void recordingStartException(Exception e) {
        Toast.makeText(this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void recordingStartMessage() {
        Toast.makeText(this, "Recording started", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void recordingCompleteMessage() {
        Toast.makeText(this, "Recording Completed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void playLastRecordAudioException(Exception e) {
        Toast.makeText(this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void playLastRecordAudioMessage() {
        Toast.makeText(this, "Recording Playing", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void stopPlayingRecordingMessage() {
        Toast.makeText(this, "Recording Playing stop", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void buttonStartEnable() {

        buttonStart.setEnabled(true);
    }

    @Override
    public void buttonStartDisable() {
        buttonStart.setEnabled(false);
    }

    @Override
    public void buttonStopEnable() {

        buttonStop.setEnabled(true);
    }

    @Override
    public void buttonStopDisable() {
        buttonStop.setEnabled(false);
    }

    @Override
    public void buttonPlayLastRecordAudioEnable() {
        buttonPlayLastRecordAudio.setEnabled(true);

    }

    @Override
    public void buttonPlayLastRecordAudioDisable() {
        buttonPlayLastRecordAudio.setEnabled(false);


    }

    @Override
    public void buttonStopPlayingRecordingEnable() {
        buttonStopPlayingRecording.setEnabled(true);

    }

    @Override
    public void buttonStopPlayingRecordingDisable() {
        buttonStopPlayingRecording.setEnabled(false);

    }
}
