package com.example.pial.audiomvpmodel.Root;

import android.app.Application;
import android.media.MediaPlayer;
import android.media.MediaRecorder;

import com.example.pial.audiomvpmodel.Audio.AudioInterface;
import com.example.pial.audiomvpmodel.Audio.AudioModel;
import com.example.pial.audiomvpmodel.Audio.AudioModule;
import com.example.pial.audiomvpmodel.Audio.AudioRepository;

/**
 * Created by pial on 7/27/17.
 */

public class App extends Application {

    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        component=DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .audioModule(new AudioModule(new AudioModel(new AudioRepository()),new AudioRepository(),new MediaRecorder(),new MediaPlayer()))
                .build();
    }
    public ApplicationComponent getComponent()
    {
        return component;
    }
}
