package com.example.pial.audiomvpmodel.Root;

import android.app.Application;

import com.example.pial.audiomvpmodel.Audio.AudioModule;
import com.example.pial.audiomvpmodel.Audio.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by pial on 7/27/17.
 */
@Singleton
@Component(modules = {ApplicationModule.class, AudioModule.class})
public interface ApplicationComponent {
    void inject(MainActivity target);
}
