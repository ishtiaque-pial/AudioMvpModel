package com.example.pial.audiomvpmodel.Audio;

import android.media.MediaPlayer;
import android.media.MediaRecorder;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by pial on 7/27/17.
 */
public class AudioPresenterTest {

    private AudioInterface.presenter presenter;
    private AudioInterface.model model;
    private AudioInterface.view view;
    private MediaRecorder mediaRecorder;
    private MediaPlayer mediaPlayer;


    @Before
    public void setUp() throws Exception {
        view=mock(AudioInterface.view.class);
        model=mock(AudioInterface.model.class);
        mediaRecorder=mock(MediaRecorder.class);
        mediaPlayer=mock(MediaPlayer.class);
        presenter=new AudioPresenter(model,mediaRecorder,mediaPlayer);
        presenter.setView(view);

    }

    @Test
    public void onButtonStart() throws Exception {
        presenter.onButtonStart("jhhj");
        verify(view,times(1)).recordingStartMessage();
    }
    @Test
    public void onButtonStop() throws Exception {
        when(model.getAudioRecording()).thenReturn(mediaRecorder);
        presenter.onButtonStop();
        verify(view,times(1)).recordingCompleteMessage();
    }

    @Test
    public void onButtonPlayLastRecordAudio() throws Exception {
        presenter.onButtonPlayLastRecordAudio();
        verify(view,times(1)).playLastRecordAudioMessage();

    }

    @Test
    public void onButtonStopPlayingRecording() throws Exception {
        when(model.getAudioMediaPlayer()).thenReturn(mediaPlayer);
        presenter.onButtonStopPlayingRecording();
        verify(view,times(1)).stopPlayingRecordingMessage();

    }

}